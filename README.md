# Intel Image Classification

**Classification task with images provided by Intel.**

## Looking at the dataset

The dataset consists of images to classify into one of six categories : buildings, forest, glacier, mountain, sea or street.

Here are some images from the dataset :

<div align="center">
<img src="images/sample.png">
</div>

## Preprocessing the data

Our preprocessing simply consists in rescaling the data.

Then we used data generators to create data augmentation. This process artificially enlarges the dataset, it prevents the network from overfitting.

The details of the data generator are available in the notebook.

## Testing a simple CNN

I tested two handmade convolutional neural networks on this dataset.
The results are between **84%** and **88%** of accuracy for those two networks.

The learning curve of the accuracy :

<div align="center">
<img src="images/accuracyCNN.png">
</div>

## Using transfer learning with a powerful network

The last network is **ResNet50**, it achieves an accuracy of **92%** on the validation set.

The learning curve of this network is way different, as expected with transfer learning, the accuracy is quickly very high :

<div align="center">
<img src="images/accuracyResNet.png">
</div>

We can look at the normalized confusion matrix (with the diagonal filled with zeros) to know the most frequent mistakes made by the network :

<div align="center">
<img src="images/confmxResNet.png">
</div>

Let's look at some badly classified images, the first row contains picture of buildings wrongly 
classified as streets and the second row contains pictures of glaciers wrongly classified as mountains :

<div align="center">
<img src="images/errorsRN.png">
</div>


<br/>Most of the badly classified images are very ambiguous and could be placed in several different categories by a human.

The notebook contains the details of the training, the networks used, the learning curves and the additional confusion matrixes.

### Reference

Deep Residual Learning for Image Recognition - Kaiming He, Xiangyu Zhang, Shaoqing Ren and Jian Sun 
https://arxiv.org/pdf/1512.03385.pdf